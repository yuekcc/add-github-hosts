use std::{
    path::Path,
    process::{self, Command},
};

use tokio::{
    fs,
    io::{AsyncReadExt, AsyncWriteExt},
};
use zigarg::Arguments;

static SYSTEM_HOSTS_FILE: &str = r"C:\Windows\System32\drivers\etc\hosts";
static GITHUB_HOSTS: &str = "https://cdn.staticaly.com/gh/qinyihao/GitHub520@main/hosts";
static GITHUB_HOSTS_BEGIN: &str = "# GitHub520 Host Start";
static GITHUB_HOSTS_END: &str = "# GitHub520 Host End";

fn flash_dns() -> anyhow::Result<bool> {
    let status = Command::new("cmd")
        .args(["/C", "ipconfig /flushdns"])
        .status()?;

    Ok(status.success())
}

async fn fetch_github520_hosts() -> anyhow::Result<String> {
    let client = reqwest::Client::new();
    let result = client.get(GITHUB_HOSTS).send().await?.text().await?;
    Ok(result)
}

async fn fetch_system_hosts(hosts_file_path: &Path) -> anyhow::Result<String> {
    let mut system_hosts = String::new();
    {
        let mut hosts_file = fs::OpenOptions::new()
            .read(true)
            .open(hosts_file_path)
            .await?;

        hosts_file.read_to_string(&mut system_hosts).await?;
    }

    Ok(system_hosts)
}

fn combine_hosts(system_hosts: &str, github_hosts: &str) -> anyhow::Result<String> {
    let lines = system_hosts.split('\n').into_iter().map(|it| it.trim_end());

    let mut result = vec![];
    let mut should_skip: bool = false;
    for line in lines {
        if line == GITHUB_HOSTS_BEGIN {
            should_skip = true;
            continue;
        }

        if line == GITHUB_HOSTS_END {
            should_skip = false;
            continue;
        }

        if !should_skip {
            result.push(line)
        }
    }

    result.push(github_hosts);

    Ok(result.join("\n"))
}

async fn write_file(hosts_file_path: &Path, new_config: &str) -> anyhow::Result<()> {
    {
        let mut hosts_file = fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .open(hosts_file_path)
            .await?;

        hosts_file.write_all(new_config.as_bytes()).await?;
    }

    Ok(())
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let arguments = Arguments::new();

    let should_print_result = arguments.exist("-p");
    let should_update_system_settings = arguments.exist("-w");

    if should_print_result || should_update_system_settings {
        let hosts_file_path = Path::new(SYSTEM_HOSTS_FILE);

        let github_hosts = fetch_github520_hosts().await?;
        let system_hosts = fetch_system_hosts(hosts_file_path).await?;
        let updated = combine_hosts(&system_hosts, &github_hosts)?;

        if should_print_result {
            println!("{}", updated.clone());
        }

        if should_update_system_settings {
            write_file(Path::new(SYSTEM_HOSTS_FILE), &updated).await?;
            flash_dns()?;
        }
    } else {
        println!("usage: cmd -w -p");
        process::exit(0)
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_read_system_hosts() {
        let hosts = fetch_system_hosts(Path::new("hosts")).await.unwrap();
        println!("{hosts}");
    }

    #[tokio::test]
    async fn test_fetch_github520_hosts() {
        let hosts = fetch_github520_hosts().await.unwrap();
        println!("{hosts}");
    }

    #[tokio::test]
    async fn test_combine_hosts() {
        let github_hosts = fetch_github520_hosts().await.unwrap();
        let system_hosts = fetch_system_hosts(Path::new("hosts")).await.unwrap();
        let updated = combine_hosts(&system_hosts, &github_hosts).unwrap();
        println!("{updated}");
    }

    #[tokio::test]
    async fn test_update_hosts_file() {
        let github_hosts = fetch_github520_hosts().await.unwrap();
        let system_hosts = fetch_system_hosts(Path::new("hosts")).await.unwrap();
        let updated = combine_hosts(&system_hosts, &github_hosts).unwrap();
        println!("{updated}");

        write_file(Path::new("hosts"), &updated).await.unwrap();
    }

    #[test]
    fn test_flash_dns() {
        flash_dns().unwrap();
    }
}

# add-github-hosts

需要管理员权限，目前只支持 windows 系统。配合系统的计划时任务，可以配置为自动更新。

## 使用

```sh
auto-edit-hosts -w # write result to file
auto-edit-hosts -p # print result only
```

## LICENSE

MIT
